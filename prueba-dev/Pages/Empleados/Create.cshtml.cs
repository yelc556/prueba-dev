using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MySql.Data.MySqlClient;
using prueba_dev.Pages.Models;

namespace prueba_dev.Pages.Empleados
{
    public class Create : PageModel
    {

        public DBContext _context { get; set; }

        [BindProperty]
        public Empleado Empleado { get; set; }

        public Create(DBContext context) {
            _context = context;
            Empleado = new Empleado();
        }

        public void OnGet()
        {
        }

        public IActionResult OnPost()
        {
            if (Empleado.IdEmpleado == 0)
            {
                Empleado.FechaIngreso = DateTime.Now;
                Empleado.DepartamentoIddepartamento = 1;
                Empleado.PuestoIdpuesto = 1;
                Empleado.Dpi = "1";
                Empleado.Direccion = "1231";
                Empleado.Telefono = "1231";
                Empleado.NoSeguroSocial = "1321312";
                Empleado.Sueldo = 2000;

                var pNombre = new MySqlParameter("@nombre", Empleado.Nombre);
                var pApellido = new MySqlParameter("@apellido", Empleado.Apellido);
                var pDpi = new MySqlParameter("@dpi", Empleado.Dpi);
                var pDireccion = new MySqlParameter("@direccion", Empleado.Direccion);
                var pTelefono = new MySqlParameter("@telefono", Empleado.Telefono);
                var pfechaIngreso = new MySqlParameter("@fechaIngreso", ((DateTime)Empleado.FechaIngreso.Value).Year + "-" + 
                    ((DateTime)Empleado.FechaIngreso.Value).Month + "-" + ((DateTime)Empleado.FechaIngreso.Value).Day);
                var pnoSeguroSocial = new MySqlParameter("@noSeguroSocial", Empleado.NoSeguroSocial);
                var psueldo = new MySqlParameter("@sueldo", Empleado.Sueldo);
                var pfkDepartamento = new MySqlParameter("@fkDepartamento", Empleado.DepartamentoIddepartamento);
                var pfkPuesto = new MySqlParameter("@fkPuesto", Empleado.PuestoIdpuesto);

                var result = _context.Empleado.FromSqlRaw("crearEmpleado @nombre, @apellido, @dpi, @direccion, @telefono, @fechaIngreso, @noSeguroSocial, @sueldo, @fkDepartamento, @fkPuesto",
                    pNombre, pApellido, pDpi, pDireccion, pTelefono, pfechaIngreso, pnoSeguroSocial, psueldo, pfkDepartamento, pfkPuesto).AsEnumerable().FirstOrDefault();

                if (result == null)
                {

                }

            }

            return RedirectToPage("./empleados");
        }
    }
}
