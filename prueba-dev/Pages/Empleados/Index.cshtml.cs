using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.RazorPages;
using prueba_dev.Pages.Models;

namespace prueba_dev.Pages.Empleados
{
    public class IndexModel : PageModel
    {
        public DBContext _context { get; set; }
        public List<Empleado> Empleados { get; set; }

        public IndexModel(DBContext context)
        {
            _context = context;
        }

        public void OnGet()
        {
            Empleados = _context.Empleado.ToList();
        }
    }
}
