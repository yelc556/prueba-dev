﻿using System;
using System.Collections.Generic;

namespace prueba_dev.Pages.Models
{
    public partial class Empleado
    {
        public int IdEmpleado { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Dpi { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public DateTime? FechaIngreso { get; set; }
        public DateTime? FechaBaja { get; set; }
        public DateTime? FechaAumento { get; set; }
        public string NoSeguroSocial { get; set; }
        public sbyte? Activo { get; set; }
        public decimal? Sueldo { get; set; }
        public int DepartamentoIddepartamento { get; set; }
        public int PuestoIdpuesto { get; set; }

        public virtual Departamento DepartamentoIddepartamentoNavigation { get; set; }
        public virtual Puesto PuestoIdpuestoNavigation { get; set; }
    }
}
