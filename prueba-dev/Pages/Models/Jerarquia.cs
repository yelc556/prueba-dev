﻿using System;
using System.Collections.Generic;

namespace prueba_dev.Pages.Models
{
    public partial class Jerarquia
    {
        public Jerarquia()
        {
            Puesto = new HashSet<Puesto>();
        }

        public int Idjerarquia { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<Puesto> Puesto { get; set; }
    }
}
