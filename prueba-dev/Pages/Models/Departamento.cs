﻿using System;
using System.Collections.Generic;

namespace prueba_dev.Pages.Models
{
    public partial class Departamento
    {
        public Departamento()
        {
            Empleado = new HashSet<Empleado>();
        }

        public int Iddepartamento { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<Empleado> Empleado { get; set; }
    }
}
