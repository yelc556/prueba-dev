﻿using System;
using System.Collections.Generic;

namespace prueba_dev.Pages.Models
{
    public partial class Puesto
    {
        public Puesto()
        {
            Empleado = new HashSet<Empleado>();
        }

        public int Idpuesto { get; set; }
        public string Nombre { get; set; }
        public decimal? Sueldo { get; set; }
        public int JerarquiaIdjerarquia { get; set; }

        public virtual Jerarquia JerarquiaIdjerarquiaNavigation { get; set; }
        public virtual ICollection<Empleado> Empleado { get; set; }
    }
}
