﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace prueba_dev.Pages.Models
{
    public partial class DBContext : DbContext
    {
        public DBContext()
        {
        }

        public DBContext(DbContextOptions<DBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Departamento> Departamento { get; set; }
        public virtual DbSet<Empleado> Empleado { get; set; }
        public virtual DbSet<Jerarquia> Jerarquia { get; set; }
        public virtual DbSet<Puesto> Puesto { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseMySql("server=localhost;database=pruebadev;user=root;pwd=ne56r10u", x => x.ServerVersion("8.0.12-mysql"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Departamento>(entity =>
            {
                entity.HasKey(e => e.Iddepartamento)
                    .HasName("PRIMARY");

                entity.ToTable("departamento");

                entity.Property(e => e.Iddepartamento)
                    .HasColumnName("iddepartamento")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("nombre")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<Empleado>(entity =>
            {
                entity.HasKey(e => e.IdEmpleado)
                    .HasName("PRIMARY");

                entity.ToTable("empleado");

                entity.HasIndex(e => e.DepartamentoIddepartamento)
                    .HasName("fk_empleado_departamento_idx");

                entity.HasIndex(e => e.PuestoIdpuesto)
                    .HasName("fk_empleado_puesto1_idx");

                entity.Property(e => e.IdEmpleado)
                    .HasColumnName("idEmpleado")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Activo)
                    .HasColumnName("activo")
                    .HasColumnType("tinyint(4)");

                entity.Property(e => e.Apellido)
                    .HasColumnName("apellido")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.DepartamentoIddepartamento)
                    .HasColumnName("departamento_iddepartamento")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Direccion)
                    .HasColumnName("direccion")
                    .HasColumnType("varchar(200)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Dpi)
                    .HasColumnName("dpi")
                    .HasColumnType("varchar(20)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.FechaAumento)
                    .HasColumnName("fechaAumento")
                    .HasColumnType("date");

                entity.Property(e => e.FechaBaja)
                    .HasColumnName("fechaBaja")
                    .HasColumnType("date");

                entity.Property(e => e.FechaIngreso)
                    .HasColumnName("fechaIngreso")
                    .HasColumnType("date");

                entity.Property(e => e.NoSeguroSocial)
                    .HasColumnName("noSeguroSocial")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Nombre)
                    .HasColumnName("nombre")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.PuestoIdpuesto)
                    .HasColumnName("puesto_idpuesto")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Sueldo)
                    .HasColumnName("sueldo")
                    .HasColumnType("decimal(10,0)");

                entity.Property(e => e.Telefono)
                    .HasColumnName("telefono")
                    .HasColumnType("varchar(15)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.HasOne(d => d.DepartamentoIddepartamentoNavigation)
                    .WithMany(p => p.Empleado)
                    .HasForeignKey(d => d.DepartamentoIddepartamento)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_empleado_departamento");

                entity.HasOne(d => d.PuestoIdpuestoNavigation)
                    .WithMany(p => p.Empleado)
                    .HasForeignKey(d => d.PuestoIdpuesto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_empleado_puesto1");
            });

            modelBuilder.Entity<Jerarquia>(entity =>
            {
                entity.HasKey(e => e.Idjerarquia)
                    .HasName("PRIMARY");

                entity.ToTable("jerarquia");

                entity.Property(e => e.Idjerarquia)
                    .HasColumnName("idjerarquia")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Nombre)
                    .HasColumnName("nombre")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<Puesto>(entity =>
            {
                entity.HasKey(e => e.Idpuesto)
                    .HasName("PRIMARY");

                entity.ToTable("puesto");

                entity.HasIndex(e => e.JerarquiaIdjerarquia)
                    .HasName("fk_puesto_jerarquia1_idx");

                entity.Property(e => e.Idpuesto)
                    .HasColumnName("idpuesto")
                    .HasColumnType("int(11)");

                entity.Property(e => e.JerarquiaIdjerarquia)
                    .HasColumnName("jerarquia_idjerarquia")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Nombre)
                    .HasColumnName("nombre")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Sueldo)
                    .HasColumnName("sueldo")
                    .HasColumnType("decimal(10,0)");

                entity.HasOne(d => d.JerarquiaIdjerarquiaNavigation)
                    .WithMany(p => p.Puesto)
                    .HasForeignKey(d => d.JerarquiaIdjerarquia)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_puesto_jerarquia1");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
