﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using prueba_dev.Pages.Models;

namespace prueba_dev.Pages
{
    public class IndexModel : Controller
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly DBContext _context;

        public IndexModel(ILogger<IndexModel> logger, DBContext context)
        {
            _logger = logger;
            _context = context;
        }

        public ActionResult OnGet()
        {
            var empleados = _context.Puesto.ToList();
            var total = empleados.Count;
            ViewBag.Total = total;
            ViewBag.Data = empleados;
            return View(ViewBag);
        }
    }
}
